<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalizacaoTable extends Migration
{
    public function up()
    {
        Schema::create('localizacao', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('google_maps');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('localizacao');
    }
}
