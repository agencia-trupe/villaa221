<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadasTable extends Migration
{
    public function up()
    {
        Schema::create('chamadas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo');
            $table->text('texto');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('chamadas');
    }
}
