<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacosTable extends Migration
{
    public function up()
    {
        Schema::create('espacos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->text('titulo');
            $table->text('descricao');
            $table->timestamps();
        });

        Schema::create('espacos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('espaco_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('espaco_id')->references('id')->on('espacos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('espacos_imagens');
        Schema::drop('espacos');
    }
}
