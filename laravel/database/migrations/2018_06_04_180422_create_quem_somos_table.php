<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuemSomosTable extends Migration
{
    public function up()
    {
        Schema::create('quem_somos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_1');
            $table->string('imagem_1');
            $table->text('texto_2');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->string('imagem_5');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('quem_somos');
    }
}
