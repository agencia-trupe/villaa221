<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunidadeTable extends Migration
{
    public function up()
    {
        Schema::create('comunidade', function (Blueprint $table) {
            $table->increments('id');
            $table->text('abertura');
            $table->string('imagem_1');
            $table->text('texto_1');
            $table->text('texto_2');
            $table->string('imagem_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('comunidade');
    }
}
