<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComodidadesTable extends Migration
{
    public function up()
    {
        Schema::create('comodidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('comodidades');
    }
}
