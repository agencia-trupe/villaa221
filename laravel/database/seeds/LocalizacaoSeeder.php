<?php

use Illuminate\Database\Seeder;

class LocalizacaoSeeder extends Seeder
{
    public function run()
    {
        DB::table('localizacao')->insert([
            'texto' => '',
            'google_maps' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
        ]);
    }
}
