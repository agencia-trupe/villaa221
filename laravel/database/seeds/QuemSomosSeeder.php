<?php

use Illuminate\Database\Seeder;

class QuemSomosSeeder extends Seeder
{
    public function run()
    {
        DB::table('quem_somos')->insert([
            'texto_1' => '',
            'imagem_1' => '',
            'texto_2' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
            'imagem_5' => '',
        ]);
    }
}
