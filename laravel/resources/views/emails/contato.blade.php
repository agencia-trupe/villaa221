<!DOCTYPE html>
<html>
<head>
    <title>[CONTATO] {{ $config->nome_do_site }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
@if($telefone)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
@endif
@if($empresa)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
@endif
@if($local)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Local:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $local }}</span><br>
@endif
@if($periodo)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Período desejado:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $periodo }}</span><br>
@endif
@if($notas_adicionais)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Notas adicionais:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $notas_adicionais }}</span>
@endif
</body>
</html>
