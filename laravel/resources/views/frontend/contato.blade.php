@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <div class="endereco">
                <p>{!! $contato->endereco !!}</p>
            </div>

            <form action="{{ route('contato.post') }}" method="POST">
                {!! csrf_field() !!}

                <h2>Interessado?</h2>
                <p>Preencha o formulário abaixo e nossa equipe entrará em contato diretamente com você.</p>

                @if(session('success'))
                   <div class="aviso enviado">Mensagem enviada com sucesso!</div>
                @endif

                @if($errors->any())
                    <div class="aviso erros">
                        @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                        @endforeach
                    </div>
                @endif

                <div class="row">
                    <input type="text" name="nome" placeholder="Nome" value="{{ old('nome') }}" required>
                    <input type="text" name="empresa" placeholder="Empresa" value="{{ old('empresa') }}" required>
                </div>
                <div class="row">
                    <input type="email" name="email" placeholder="E-mail" value="{{ old('email') }}" required>
                    <input type="text" name="local" placeholder="Local" value="{{ old('local') }}" required>
                </div>
                <div class="row">
                    <input type="text" name="telefone" placeholder="Telefone" value="{{ old('telefone') }}" required>
                    <input type="text" name="periodo" placeholder="Período desejado" value="{{ old('periodo') }}" required>
                </div>
                <div class="row">
                    <textarea name="notas_adicionais" placeholder="Notas adicionais">{{ old('notas_adicionais') }}</textarea>
                </div>

                <input type="submit" value="ENVIAR">
            </form>
        </div>

        <div class="mapa">{!! $localizacao->google_maps !!}</div>
    </div>

@endsection
