<a href="{{ route('conceito') }}" @if(Tools::isActive('conceito')) class="active" @endif>
    Conceito
</a>
<a href="{{ route('espacos') }}" @if(Tools::isActive('espacos')) class="active" @endif>
    Espaços
</a>
<a href="{{ route('comodidades') }}" @if(Tools::isActive('comodidades')) class="active" @endif>
    Comodidades
</a>
<a href="{{ route('comunidade') }}" @if(Tools::isActive('comunidade')) class="active" @endif>
    Comunidade
</a>
<a href="{{ route('quemSomos') }}" @if(Tools::isActive('quemSomos')) class="active" @endif>
    Quem Somos
</a>
<a href="{{ route('localizacao') }}" @if(Tools::isActive('localizacao')) class="active" @endif>
    Localização
</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>
    Contato
</a>
