    <footer>
        <div class="center">
            <div class="links">
                @include('frontend.common.nav')
            </div>
            <div class="marca">
                <img src="{{ asset('assets/img/layout/villa-a-221-footer.png') }}" alt="">
                @include('frontend.common.social')
            </div>
            <div class="informacoes">
                <p class="endereco">{!! $contato->endereco !!}</p>
                <p class="copyright">
                    © {{ date('Y') }} {{ $config->nome_do_site }} - Todos os direitos reservados.
                    <br>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
