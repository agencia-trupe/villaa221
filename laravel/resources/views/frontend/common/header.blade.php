    <header @if(Route::currentRouteName() == 'home') class="home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
                @include('frontend.common.social')
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <div id="nav-mobile">
            @include('frontend.common.nav')
            @include('frontend.common.social')
        </div>
    </header>
