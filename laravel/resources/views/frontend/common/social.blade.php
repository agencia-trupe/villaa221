<div class="social">
    @if($contato->facebook)
    <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
    @endif
    @if($contato->instagram)
    <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
    @endif
</div>
