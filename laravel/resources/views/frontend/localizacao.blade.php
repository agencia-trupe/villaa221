@extends('frontend.common.template')

@section('content')

    <div class="localizacao">
        <div class="texto center">
            {!! $localizacao->texto !!}
        </div>

        <div class="mapa">
            {!! $localizacao->google_maps !!}
        </div>

        <div class="imagens">
            <img src="{{ asset('assets/img/localizacao/'.$localizacao->imagem_1) }}" alt="">
            <img src="{{ asset('assets/img/localizacao/'.$localizacao->imagem_2) }}" alt="">
            <img src="{{ asset('assets/img/localizacao/'.$localizacao->imagem_3) }}" alt="">
        </div>
    </div>

@endsection
