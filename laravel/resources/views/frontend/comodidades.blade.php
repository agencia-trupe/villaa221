@extends('frontend.common.template')

@section('content')

    <div class="comodidades">
        <div class="titulo">
            <div class="center">
                COMODIDADES INCLUSAS
            </div>
        </div>

        <div class="icones center">
            @foreach($comodidades as $comodidade)
            <div>
                <img src="{{ asset('assets/img/comodidades/'.$comodidade->imagem) }}" alt="">
                <span>{{ $comodidade->texto }}</span>
            </div>
            @endforeach
        </div>
    </div>

@endsection
