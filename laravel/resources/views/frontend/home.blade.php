@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
            @if($banner->link)
                <a href="{{ $banner->link }}" class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            @else
                <div class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            @endif
                @if($banner->texto)
                    <div class="center">
                        <div class="texto">
                            <span>{!! $banner->texto !!}</span>
                        </div>
                    </div>
                @endif
            @if($banner->link)
                </a>
            @else
                </div>
            @endif
        @endforeach
    </div>

    <div class="chamadas">
        <div class="center">
            @foreach($chamadas as $chamada)
                @if($chamada->link)
                    <a href="{{ $chamada->link }}" class="chamada">
                        <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" alt="">
                        <h3>{{ $chamada->titulo }}</h3>
                        <p>{!! $chamada->texto !!}</p>
                    </a>
                @else
                    <div class="chamada">
                        <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" alt="">
                        <h3>{{ $chamada->titulo }}</h3>
                        <p>{!! $chamada->texto !!}</p>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

@endsection
