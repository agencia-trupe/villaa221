@extends('frontend.common.template')

@section('content')

    <div class="comunidade">
        <div class="abertura center">
            {!! $comunidade->abertura !!}
        </div>

        <div class="linhas">
            <div class="linha">
                <div class="center">
                    <img src="{{ asset('assets/img/comunidade/'.$comunidade->imagem_1) }}" alt="">
                    <div class="texto">{!! $comunidade->texto_1 !!}</div>
                </div>
            </div>
            <div class="linha">
                <div class="center">
                    <img src="{{ asset('assets/img/comunidade/'.$comunidade->imagem_2) }}" alt="">
                    <div class="texto">{!! $comunidade->texto_2 !!}</div>
                </div>
            </div>
        </div>
    </div>

@endsection
