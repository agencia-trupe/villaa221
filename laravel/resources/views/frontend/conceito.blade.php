@extends('frontend.common.template')

@section('content')

    <div class="conceito">
        <div class="imagens">
            <img src="{{ asset('assets/img/conceito/'.$conceito->imagem_1) }}" alt="">
            <img src="{{ asset('assets/img/conceito/'.$conceito->imagem_2) }}" alt="">
            <img src="{{ asset('assets/img/conceito/'.$conceito->imagem_3) }}" alt="">
        </div>

        <div class="texto center">
            {!! $conceito->texto !!}
        </div>
    </div>

@endsection
