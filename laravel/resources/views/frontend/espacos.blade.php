@extends('frontend.common.template')

@section('content')

    <div class="espacos">
        <div class="abertura center">
            <h2>Espaços</h2>
            {!! $texto->texto !!}
        </div>

        <div class="espacos-lista">
            @foreach($espacos as $espaco)
            <div class="espaco">
                <div class="titulo">
                    <div class="center">
                        <h3>{!! $espaco->titulo !!}</h3>
                    </div>
                </div>
                <div class="center">
                    <div class="texto">{!! $espaco->descricao !!}</div>
                    <div class="imagens">
                        @foreach($espaco->imagens as $imagem)
                        <img src="{{ asset('assets/img/espacos/imagens/'.$imagem->imagem) }}" alt="">
                        @endforeach
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
