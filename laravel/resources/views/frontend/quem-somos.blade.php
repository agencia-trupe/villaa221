@extends('frontend.common.template')

@section('content')

    <div class="quem-somos">
        <div class="abertura">
            <div class="center">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_1) }}" alt="">
                <div class="texto">{!! $quemSomos->texto_1 !!}</div>
            </div>
        </div>
        <div class="center">
            <div class="texto">{!! $quemSomos->texto_2 !!}</div>
            <div class="imagens">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_2) }}" alt="">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_3) }}" alt="">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_4) }}" alt="">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_5) }}" alt="">
            </div>
        </div>
    </div>

@endsection
