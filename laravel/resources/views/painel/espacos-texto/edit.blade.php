@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.espacos.index') }}" title="Voltar para Espaços" class="btn btn-sm btn-default">
        &larr; Voltar para Espaços    </a>

    <legend>
        <h2><small>Espaços /</small> Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.espacos-texto.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.espacos-texto.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
