@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Conceito</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.conceito.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.conceito.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
