<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.chamadas*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
    </li>
    <li @if(str_is('painel.conceito*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.conceito.index') }}">Conceito</a>
    </li>
	<li @if(str_is('painel.espacos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.espacos.index') }}">Espaços</a>
	</li>
	<li @if(str_is('painel.comodidades*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.comodidades.index') }}">Comodidades</a>
	</li>
	<li @if(str_is('painel.comunidade*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.comunidade.index') }}">Comunidade</a>
	</li>
	<li @if(str_is('painel.quem-somos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.quem-somos.index') }}">Quem Somos</a>
	</li>
	<li @if(str_is('painel.localizacao*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.localizacao.index') }}">Localização</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
