@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Comunidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.comunidade.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.comunidade.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
