@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Chamadas /</small> Editar Chamada</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.chamadas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.chamadas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
