@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Chamadas /</small> Adicionar Chamada</h2>
    </legend>

    {!! Form::open(['route' => 'painel.chamadas.store', 'files' => true]) !!}

        @include('painel.chamadas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
