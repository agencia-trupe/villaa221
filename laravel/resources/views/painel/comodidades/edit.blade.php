@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Comodidades /</small> Editar Comodidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.comodidades.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.comodidades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
