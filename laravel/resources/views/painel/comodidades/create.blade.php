@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Comodidades /</small> Adicionar Comodidade</h2>
    </legend>

    {!! Form::open(['route' => 'painel.comodidades.store', 'files' => true]) !!}

        @include('painel.comodidades.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
