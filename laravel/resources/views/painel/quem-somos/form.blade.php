@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_1', 'Texto 1') !!}
            {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            @if($registro->imagem_1)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitulo']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            @if($registro->imagem_2)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            @if($registro->imagem_3)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4') !!}
            @if($registro->imagem_4)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_5', 'Imagem 5') !!}
            @if($registro->imagem_5)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
