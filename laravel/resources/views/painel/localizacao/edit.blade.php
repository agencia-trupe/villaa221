@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Localização</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.localizacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.localizacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
