@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Espaços /</small> Adicionar Espaço</h2>
    </legend>

    {!! Form::open(['route' => 'painel.espacos.store', 'files' => true]) !!}

        @include('painel.espacos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
