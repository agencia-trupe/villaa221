@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Espaços /</small> Editar Espaço</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.espacos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.espacos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
