<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EspacosTextoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
        ];
    }
}
