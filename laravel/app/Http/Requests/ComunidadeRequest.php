<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ComunidadeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'abertura' => 'required',
            'imagem_1' => 'image',
            'texto_1' => 'required',
            'texto_2' => 'required',
            'imagem_2' => 'image',
        ];
    }
}
