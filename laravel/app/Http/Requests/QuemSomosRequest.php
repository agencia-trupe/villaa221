<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuemSomosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_1' => 'required',
            'imagem_1' => 'image',
            'texto_2' => 'required',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
            'imagem_5' => 'image',
        ];
    }
}
