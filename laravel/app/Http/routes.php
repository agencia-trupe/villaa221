<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('conceito', 'ConceitoController@index')->name('conceito');
    Route::get('espacos', 'EspacosController@index')->name('espacos');
    Route::get('comodidades', 'ComodidadesController@index')->name('comodidades');
    Route::get('comunidade', 'ComunidadeController@index')->name('comunidade');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quemSomos');
    Route::get('localizacao', 'LocalizacaoController@index')->name('localizacao');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('espacos-texto', 'EspacosTextoController', ['only' => ['index', 'update']]);
		Route::resource('espacos', 'EspacosController');
		Route::get('espacos/{espacos}/imagens/clear', [
			'as'   => 'painel.espacos.imagens.clear',
			'uses' => 'EspacosImagensController@clear'
		]);
		Route::resource('espacos.imagens', 'EspacosImagensController', ['parameters' => ['imagens' => 'imagens_espacos']]);
		Route::resource('chamadas', 'ChamadasController');
		Route::resource('banners', 'BannersController');
		Route::resource('conceito', 'ConceitoController', ['only' => ['index', 'update']]);
		Route::resource('comodidades', 'ComodidadesController');
		Route::resource('comunidade', 'ComunidadeController', ['only' => ['index', 'update']]);
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
		Route::resource('localizacao', 'LocalizacaoController', ['only' => ['index', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
