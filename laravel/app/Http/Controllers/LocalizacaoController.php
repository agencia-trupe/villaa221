<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Localizacao;

class LocalizacaoController extends Controller
{
    public function index()
    {
        $localizacao = Localizacao::first();

        return view('frontend.localizacao', compact('localizacao'));
    }
}
