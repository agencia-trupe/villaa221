<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Espaco;
use App\Models\EspacosTexto;

class EspacosController extends Controller
{
    public function index()
    {
        $texto   = EspacosTexto::first();
        $espacos = Espaco::ordenados()->get();

        return view('frontend.espacos', compact('texto', 'espacos'));
    }
}
