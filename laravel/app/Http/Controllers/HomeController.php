<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Chamada;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $chamadas = Chamada::ordenados()->get();

        return view('frontend.home', compact('banners', 'chamadas'));
    }
}
