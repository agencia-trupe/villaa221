<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EspacosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Espaco;
use App\Models\EspacoImagem;

use App\Helpers\CropImage;

class EspacosImagensController extends Controller
{
    public function index(Espaco $registro)
    {
        $imagens = EspacoImagem::espaco($registro->id)->ordenados()->get();

        return view('painel.espacos.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Espaco $registro, EspacoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Espaco $registro, EspacosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = EspacoImagem::uploadImagem();
            $input['espaco_id'] = $registro->id;

            $imagem = EspacoImagem::create($input);

            $view = view('painel.espacos.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Espaco $registro, EspacoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.espacos.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Espaco $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.espacos.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
