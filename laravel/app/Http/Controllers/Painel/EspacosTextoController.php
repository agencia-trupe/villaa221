<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EspacosTextoRequest;
use App\Http\Controllers\Controller;

use App\Models\EspacosTexto;

class EspacosTextoController extends Controller
{
    public function index()
    {
        $registro = EspacosTexto::first();

        return view('painel.espacos-texto.edit', compact('registro'));
    }

    public function update(EspacosTextoRequest $request, EspacosTexto $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.espacos-texto.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
