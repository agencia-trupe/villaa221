<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ComunidadeRequest;
use App\Http\Controllers\Controller;

use App\Models\Comunidade;

class ComunidadeController extends Controller
{
    public function index()
    {
        $registro = Comunidade::first();

        return view('painel.comunidade.edit', compact('registro'));
    }

    public function update(ComunidadeRequest $request, Comunidade $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Comunidade::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Comunidade::upload_imagem_2();

            $registro->update($input);

            return redirect()->route('painel.comunidade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
