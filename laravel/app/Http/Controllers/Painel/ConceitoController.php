<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConceitoRequest;
use App\Http\Controllers\Controller;

use App\Models\Conceito;

class ConceitoController extends Controller
{
    public function index()
    {
        $registro = Conceito::first();

        return view('painel.conceito.edit', compact('registro'));
    }

    public function update(ConceitoRequest $request, Conceito $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Conceito::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Conceito::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = Conceito::upload_imagem_3();

            $registro->update($input);

            return redirect()->route('painel.conceito.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
