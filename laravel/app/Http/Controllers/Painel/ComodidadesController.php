<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ComodidadesRequest;
use App\Http\Controllers\Controller;

use App\Models\Comodidade;

class ComodidadesController extends Controller
{
    public function index()
    {
        $registros = Comodidade::ordenados()->get();

        return view('painel.comodidades.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.comodidades.create');
    }

    public function store(ComodidadesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Comodidade::upload_imagem();

            Comodidade::create($input);

            return redirect()->route('painel.comodidades.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Comodidade $registro)
    {
        return view('painel.comodidades.edit', compact('registro'));
    }

    public function update(ComodidadesRequest $request, Comodidade $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Comodidade::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.comodidades.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Comodidade $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.comodidades.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
