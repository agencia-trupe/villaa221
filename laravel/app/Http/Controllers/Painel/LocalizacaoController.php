<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LocalizacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\Localizacao;

class LocalizacaoController extends Controller
{
    public function index()
    {
        $registro = Localizacao::first();

        return view('painel.localizacao.edit', compact('registro'));
    }

    public function update(LocalizacaoRequest $request, Localizacao $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Localizacao::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Localizacao::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = Localizacao::upload_imagem_3();

            $registro->update($input);

            return redirect()->route('painel.localizacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
