<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EspacosRequest;
use App\Http\Controllers\Controller;

use App\Models\Espaco;

class EspacosController extends Controller
{
    public function index()
    {
        $registros = Espaco::ordenados()->get();

        return view('painel.espacos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.espacos.create');
    }

    public function store(EspacosRequest $request)
    {
        try {

            $input = $request->all();

            Espaco::create($input);

            return redirect()->route('painel.espacos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Espaco $registro)
    {
        return view('painel.espacos.edit', compact('registro'));
    }

    public function update(EspacosRequest $request, Espaco $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.espacos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Espaco $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.espacos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
