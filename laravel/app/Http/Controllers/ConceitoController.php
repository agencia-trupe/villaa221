<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Conceito;

class ConceitoController extends Controller
{
    public function index()
    {
        $conceito = Conceito::first();

        return view('frontend.conceito', compact('conceito'));
    }
}
