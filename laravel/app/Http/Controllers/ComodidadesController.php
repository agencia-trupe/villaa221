<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Comodidade;

class ComodidadesController extends Controller
{
    public function index()
    {
        $comodidades = Comodidade::ordenados()->get();

        return view('frontend.comodidades', compact('comodidades'));
    }
}
