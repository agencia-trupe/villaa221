<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\Localizacao;
use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    public function index()
    {
        $localizacao = Localizacao::first();

        return view('frontend.contato', compact('localizacao'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $input, function ($message) use ($request, $contato) {
                $message->to($contato->email, 'Villa A 221')
                        ->subject('[CONTATO] Villa A 221')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return redirect()->back()->with('success', true);
    }
}
