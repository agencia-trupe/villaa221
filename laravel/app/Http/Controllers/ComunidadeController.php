<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Comunidade;

class ComunidadeController extends Controller
{
    public function index()
    {
        $comunidade = Comunidade::first();

        return view('frontend.comunidade', compact('comunidade'));
    }
}
