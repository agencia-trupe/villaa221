<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class EspacoImagem extends Model
{
    protected $table = 'espacos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeEspaco($query, $id)
    {
        return $query->where('espaco_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/espacos/imagens/thumbs/'
            ],
            [
                'width'   => 580,
                'height'  => 390,
                'path'    => 'assets/img/espacos/imagens/'
            ]
        ]);
    }
}
