<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Comunidade extends Model
{
    protected $table = 'comunidade';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 580,
            'height' => 390,
            'path'   => 'assets/img/comunidade/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 580,
            'height' => 390,
            'path'   => 'assets/img/comunidade/'
        ]);
    }

}
