<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class EspacosTexto extends Model
{
    protected $table = 'espacos_texto';

    protected $guarded = ['id'];

}
