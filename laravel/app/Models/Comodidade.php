<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Comodidade extends Model
{
    protected $table = 'comodidades';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'       => 70,
            'height'      => 70,
            'transparent' => true,
            'upsize'      => true,
            'path'        => 'assets/img/comodidades/'
        ]);
    }
}
