<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Conceito extends Model
{
    protected $table = 'conceito';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 640,
            'height' => 335,
            'path'   => 'assets/img/conceito/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 640,
            'height' => 335,
            'path'   => 'assets/img/conceito/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 640,
            'height' => 335,
            'path'   => 'assets/img/conceito/'
        ]);
    }

}
