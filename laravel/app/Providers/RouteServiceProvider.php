<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('espacos-texto', 'App\Models\EspacosTexto');
		$router->model('espacos', 'App\Models\Espaco');
		$router->model('imagens_espacos', 'App\Models\EspacoImagem');
		$router->model('chamadas', 'App\Models\Chamada');
		$router->model('banners', 'App\Models\Banner');
		$router->model('conceito', 'App\Models\Conceito');
		$router->model('comodidades', 'App\Models\Comodidade');
		$router->model('comunidade', 'App\Models\Comunidade');
		$router->model('quem-somos', 'App\Models\QuemSomos');
		$router->model('localizacao', 'App\Models\Localizacao');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
